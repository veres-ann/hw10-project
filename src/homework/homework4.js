/**
 * kolobok
 * @param {string} name
 * @returns {string}
 */

/**
 * kolobok
 * @param {string} name
 * @returns {string}
 */

const kolobok = (personage) => {
  let result = null;

  switch (personage) {
    case 'Бабушка':
      console.log('Я от бабушки ушёл');
      result = 'Я от бабушки ушёл';
      break;
    case 'Дедушка':
      console.log('Я от дедушки ушёл');
      result = 'Я от дедушки ушёл';
      break;
    case 'Волк':
      console.log('Я от волка ушёл');
      result = 'Я от волка ушёл';
      break;
    case 'Медведь':
      console.log('Я от медведя ушёл');
      result = 'Я от медведя ушёл';
      break;
    case 'Лиса':
      console.log('Лиса меня съела');
      result = 'Лиса меня съела';
      break;
    default:
      console.log(`Персонаж, ${personage} не существует.`);
      result = `Персонаж, ${personage} не существует.`;
  }
  return result;
};

export { kolobok };

const NewYear = (personage) => {
  let result = null;

  switch (personage) {
    case 'Снегурочка':
      console.log('Снегурочка!Снегурочка!Снегурочка!');
      result = 'Снегурочка! Снегурочка! Снегурочка!';
      break;
    case 'Дед Мороз':
      console.log('Дед Мороз! Дед Мороз! Дед Мороз!');
      result = 'Дед Мороз! Дед Мороз! Дед Мороз!';
      break;
    default:
      console.log(`В этом новом году ${personage} не существует.`); // интерполяция
      result = `В этом новом году, ${personage} не существует.`;
  }
  return result;
};

export { NewYear };
