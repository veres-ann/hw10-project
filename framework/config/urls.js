const urls = {
  airportgap: 'https://airportgap.dev-tester.com',
  vikunja: 'https://try.vikunja.io',
  demo: 'https://91i.ru',
  mailboxlayer: 'https://apilayer.net',
};
export { urls };
