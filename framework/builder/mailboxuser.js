import faker from 'faker';

const PersonBuilder = function PersonBuilder() {
  this.addMail = function addMail() {
    this.login = faker.internet.email();
    return this;
  };
  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);

    const data = {};
    fields.forEach((fieldName) => {
      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    return data;
  };
};

export { PersonBuilder };
