import supertest from 'supertest';
import { urls } from '../config';

const LoginAirportGap = function LoginAirportGap() {
  this.post = async function loginAirportGap(credentials) {
    const r = await supertest(urls.airportgap).post('/api/tokens').send(credentials);
    return r;
  };
};

const LoginVikunja = function LoginVikunja() {
  this.post = async function loginVikunja(params) {
    const r = await supertest(urls.vikunja).post('/api/v1/login').send(params);
    return r;
  };
};

const LoginMailboxlayer = function LoginMailboxlayer() {
// eslint-disable-next-line no-shadow
  this.post = async function LoginMailboxlayer(credsMailboxlayer) {
    const r = await supertest(urls.mailboxlayer).post('/api/check').send(credsMailboxlayer);
    return r;
  };
};

export { LoginVikunja, LoginAirportGap, LoginMailboxlayer };
