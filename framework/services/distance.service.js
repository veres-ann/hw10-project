import supertest from 'supertest';
import { urls } from '../config';

const DistanceAirportGap = function DistanceAirportGap() {
  this.post = async function postDistanceAirportGap(distance, token) {
    const r = await supertest(urls.airportgap)
      .post('/api/airports/distance')
      .set('Authorization', `Bearer ${token}`)
      .send(distance);

    return r;
  };
};

export { DistanceAirportGap };
