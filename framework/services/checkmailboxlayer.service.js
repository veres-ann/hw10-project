import supertest from 'supertest';
import { urls } from '../config';

const CheckMail = function CheckMail() {
  this.get = async function getCheckMail(token, mail) {
    const r = await supertest(urls.mailboxlayer)
      .get(`/api/check?access_key=${token}&email=${mail}`);
    if (token) {
      let result = '';
      if (r.body.format_valid === true || r.body.format_valid === false) {
        result = r.body.format_valid;
      } else {
        result = r.body.success;
      }
      return result;
    }
    return r;
  };
};

export { CheckMail };
