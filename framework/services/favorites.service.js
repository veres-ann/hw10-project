import supertest from 'supertest';
import { urls } from '../config';

const FavAirportGap = function FavAirportGap() {
  this.post = async function postFavAirportGap(AirportId, token) {
    const r = await supertest(urls.airportgap)
      .post('/api/favorites')
      .set('Authorization', `Bearer ${token}`)
      .send(AirportId);
    return r;
  };
  this.get = async function getFavAirportGap(token) {
    const r = await supertest(urls.airportgap)
      .get('/api/favorites')
      .set('Authorization', `Bearer ${token}`);
    return r;
  };

  this.delete = async function deleteFavAirportGap(id, token) {
    const r = await supertest(urls.airportgap)
      .delete(`/api/favorites/${id}`)
      .set('Authorization', `Bearer ${token}`);
    return r;
  };
};

export { FavAirportGap };
