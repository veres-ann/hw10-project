import { Registration, CheckMail } from './services/index';

const apiProvider = () => ({
  registration: () => new Registration(),
  info: () => new Registration(),
  checkmail: () => new CheckMail(),
});

export { apiProvider };
