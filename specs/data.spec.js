import Ajv from 'ajv';
import faker from 'faker';
import Joi from '@hapi/joi';
import supertest from 'supertest';
import { test } from '@jest/globals';

import { BuildUser } from '../framework/builder/user';

import schemaAjv from '../framework/model/swagger.json';

// Хардкод вариант. Версия теста 0.0.1
test('Пользователь может зарегистрироваться в системе', async () => {
  const data = {
    login: 'string11',
    password: 'string222',
    birth_date: '2020-12-07T17:09:14.344Z',
    nick: 'string22',
  };
  const r = await supertest('https://91i.ru')
    .post('/api/v1/register')
    .send(data);
  expect(r.status)
    .toBe(200);
});

// Рандомный вариант. Версия теста 0.0.2
test('Пользователь может зарегистрироваться в системе v2', async () => {
  const data = {
    login: faker.internet.email(),
    password: faker.vehicle.vin(),
    birth_date: '2020-12-07T17:09:14.344Z',
    nick: 'string22',
  };
  const r = await supertest('https://91i.ru')
    .post('/api/v1/register')
    .send(data);
  expect(r.status)
    .toBe(200);
});

// Рандомный вариант + Билдер. Версия теста 0.0.3
test('Пользователь может зарегистрироваться в системе v3', async () => {
  const data = new BuildUser().get('new');
  const r = await supertest('https://91i.ru')
    .post('/api/v1/register')
    .send(data);
  expect(r.status)
    .toBe(200);
});
// Рандомный вариант + Билдер + валидация json схемы + AJV. Версия теста 0.0.4
test('Пользователь может зарегистрироваться в системе v4', async () => {
  const ajv = new Ajv({
    $data: true, logger: console, allErrors: true, verbose: true,
  });

  const data = new BuildUser().get('new');
  const { body, status } = await supertest('https://91i.ru')
    .post('/api/v1/register')
    .send(data);

  ajv.addSchema(schemaAjv, 'swagger.json');
  // eslint-disable-next-line no-unused-vars
  const valid = ajv.validate({ $ref: 'swagger.json#/components/schemas/FullUserInfo' }, body);
  if (valid) console.log('Valid!');
  else console.log(`Invalid: ${ajv.errorsText(valid.errors)}`);

  expect(status)
    .toBe(200);
});

// Рандомный вариант + Билдер + валидация json схемы JOI. Версия теста 0.0.4

test('Пользователь может зарегистрироваться в системе v5', async () => {
  const data = new BuildUser().get('new');

  const schema = Joi.object()
    .keys({
      access_token: Joi.string(),
    }).max(2).required();
  const {
    // eslint-disable-next-line no-unused-vars
    body,
    status,
  } = await supertest('https://91i.ru')
    .post('/api/v1/register')
    .send(data);

  // eslint-disable-next-line no-unused-vars
  const { error } = schema.validate(body);
  // console.log(error);

  expect(status)
    .toBe(200);
});
