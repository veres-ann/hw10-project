import { test } from '@jest/globals';
import { urls, credentials } from '../framework/config/index';
import { DistanceAirportGap } from '../framework/services/distance.service';
import { FavAirportGap } from '../framework/services/favorites.service';
import { LoginAirportGap } from '../framework/services/login.service';

describe('Airport Gap Distance tests', () => {
  test('Distance from KIX to NRT = 491', async () => {
    const r = await new LoginAirportGap().post(credentials);
    const { token } = r.body;
    const distance = {
      from: 'KIX',
      to: 'NRT',
    };
    const Resp = await new DistanceAirportGap()
      .post(distance, token);
    expect(Resp.body.data.attributes.kilometers).toBe(490.8053652969216);
  });

  test('Negative test: Distance from \'\' to \'\' = 422 Status', async () => {
    const r = await new LoginAirportGap().post(credentials);
    const { token } = r.body;
    const distance = {
      from: '',
      to: '',
    };

    const Resp = await new DistanceAirportGap()
      .post(distance, token);

    expect(Resp.status).toBe(422);
  });
});

describe('Airport Gap: Favourits testing', () => {
  test('Add airport to Favourits', async () => {
    const r = await new LoginAirportGap().post(credentials);
    const { token } = r.body;

    const AirportId = {
      airport_id: 'GKA',
    };

    const Resp = await new FavAirportGap()
      .post(AirportId, token);
    const data = Resp.body;
    const FavId = JSON.parse(JSON.stringify(data)).data.attributes.airport.iata;
    // JSON.stringify(data) - переводим в строку ответ запроса.
    // JSON.parse - парсит строку и может ытащить из неё нужные данные
    expect(AirportId.airport_id).toBe(FavId);
  });

  test('Delete all existing airports from favourits', async () => {
    const r = await new LoginAirportGap().post(credentials);
    const { token } = r.body;
    console.log(token);
    const Resp = await new FavAirportGap().get(token);
    const data = Resp.body;

    for (let i = 0; i < data.data.length; i += 1) {
      console.log(urls.airportgap);
      const { id } = data.data[i];
      // eslint-disable-next-line no-await-in-loop
      const DelResp = await new FavAirportGap().delete(id, token);
      expect(DelResp.statusCode).toBe(204);
    }
  });
});
