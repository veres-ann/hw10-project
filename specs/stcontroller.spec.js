import { test } from '@jest/globals';
import LoginVikunja from '../framework/services/login.service';

test('Сервис', async () => {
  const params = {
    username: 'demo',
    password: 'demo',
  };

  const r = await LoginVikunja.post(params);
  expect(r.status)
    .toBe(200);
});
