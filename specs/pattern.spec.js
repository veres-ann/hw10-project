import faker from 'faker';
import { apiProvider } from '../framework';

const PersonBuilder = function PersonBuilder() {
  this.addBirthdate = function addLogin() {
    this.birth_date = '2020-12-07T17:09:14.344Z';
    return this;
  };
  this.addLogin = function addLogin() {
    this.login = faker.internet.email();
    return this;
  };
  this.addNick = function addNick() {
    this.nick = 'faker.internet.email()';
    return this;
  };
  this.addPassword = function addPassword() {
    this.password = faker.vehicle.vin();
    return this;
  };
  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    const data = {};
    fields.forEach((fieldName) => {
      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    return data;
  };
};

test('Provider demo', async () => {
  const demo = new PersonBuilder();
  const demoLogin = demo
    .addLogin()
    .addBirthdate()
    .addNick()
    .addPassword()
    .generate();

  const r = await apiProvider().registration().post(demoLogin);
  expect(r.status).toBe(200);
});
