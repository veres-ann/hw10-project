import { test } from '@jest/globals';
import { credentials } from '../framework/config/index';
import { DistanceAirportGap } from '../framework/services/distance.service';
import { LoginAirportGap } from '../framework/services/login.service';

describe('Airport Gap Distance tests', () => {
  test.each`
      from          | to            | ExpectedDistance       
     ${'KIX'}       | ${'NRT'}      | ${491}                 
     ${'GKA'}       | ${'MAG'}      | ${107}                
     ${'YBR'}       | ${'YCB'}      | ${2153}                
     ${'HGU'}       | ${'LAE'}      | ${281}                 
     ${'POM'}       | ${'WWK'}      | ${760}                 
     ${'UAK'}       | ${'GOH'}      | ${464}                 
     `(' $from и $to расстояние $ExpectedDistance',
    async ({ from, to, ExpectedDistance }) => {
      const r = await new LoginAirportGap().post(credentials);
      const { token } = r.body;
      const distance = {
        from,
        to,
      };
      const Resp = await new DistanceAirportGap()
        .post(distance, token);
      const data = Resp.body;

      const kilometers = Number(JSON.parse(JSON.stringify(data))
        .data.attributes.kilometers.toFixed(0));
      expect(kilometers).toEqual(ExpectedDistance);
    });
});
