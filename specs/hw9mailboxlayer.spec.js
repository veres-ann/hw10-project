import { test } from '@jest/globals';
import { apiProvider } from '../framework';
import { credsMailboxlayer } from '../framework/config/index';
import { PersonBuilder } from '../framework/builder/mailboxuser';

describe('Mailboxlayer tests', () => {
  describe('Check mails param test', () => {
    test.each`
       testmail                |     formatValid
      ${'ann@yahoo.com'}       |     ${true}   
      ${'noemail'}             |     ${false}           
      ${''}                    |     ${false}          
      ${'    '}                |     ${false}         
      ${'@mil.com'}            |     ${false}         
      ${'mail.com'}            |     ${false} 
       
     `('Email $testmail is checked and Format Valid is $formatValid',
      async ({ testmail, formatValid }) => {
        const r = await apiProvider().checkmail().get(credsMailboxlayer, testmail);
        expect(r).toBe(formatValid);
      });
  });

  test('Check valid Email', async () => {
    const demo = new PersonBuilder();
    const genMail = demo
      .addMail()
      .generate();

    const r = await apiProvider().checkmail().get(credsMailboxlayer, genMail.login);
    expect(r).toBe(true);
  });
  test('Check missing Api key', async () => {
    const demo = new PersonBuilder();
    const genMail = demo
      .addMail()
      .generate();

    const r = await apiProvider().checkmail().get(null, genMail);
    expect(r.body.error.type).toBe('invalid_access_key');
  });
});
